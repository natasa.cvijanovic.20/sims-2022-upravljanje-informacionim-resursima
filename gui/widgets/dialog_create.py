from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QDialog, QFormLayout, QLabel, QLineEdit, QDialogButtonBox, QMessageBox

class DialogCreate(QDialog):
    def __init__(self, parent=None, header_data=[]):
        super().__init__()
        
        self.setWindowTitle("Rukovalac Informacionim Resursima - Create")
        self.setWindowIcon(QIcon("resources/icons/document--plus.png"))
        self.resize(400, 100)

        self.parent = parent

        self.id = ""
        self.header_data = []
        for a in header_data:
            self.header_data.append(a)
        if "id" in self.header_data:
            self.id = "id"
            self.header_data.remove("id")
        self.form_layout = QFormLayout(self)
        self.info_label = QLabel("Enter the values:")
        self.form_layout.addRow(self.info_label)

        self.first, self.second, self.third, self.fourth, self.fifth, self.sixth = None, None, None, None, None, None
        self.fields= [self.first, self.second, self.third, self.fourth, self.fifth, self.sixth]

        self.first_l, self.second_l, self.third_l, self.fourth_l, self.fifth_l, self.sixth_l = None, None, None, None, None, None
        self.labels= [self.first_l, self.second_l, self.third_l, self.fourth_l, self.fifth_l, self.sixth_l]

        for i, column in enumerate(self.header_data):
            self.labels[i] = QLabel(column)
            self.fields[i] = QLineEdit()
            self.form_layout.addRow(self.labels[i], self.fields[i])

        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.form_layout.addRow(self.button_box)
        self.setLayout(self.form_layout)

    # potvrda kreiranja ir
    def accept(self):
        data = {}
        for i in range(len(self.header_data)):
            data[self.labels[i].text()] = self.fields[i].text()

        flag, dat = self.parent.checkDataDict(data)
        
        if not self.parent.checkKeyValidity(dat, self.id):
            QMessageBox.warning(self, "Failed", "Unique fields must be respected")
        else:
            if not flag:
                QMessageBox.warning(self,"Warning","Incorrect data types entered. The invalid values have been overwritten")
            else:
                QMessageBox.information(self,"Successfully added","Success!")

            self.parent.createIR(dat)
        self.close()