from PySide2.QtWidgets import QMenuBar, QMenu, QAction

class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        # poziv nadredjenog inicijalizatora (QMainWindow)
        super().__init__(parent)
        self.parent = parent
        # kreiranje osnovnih menija
        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit", self)
        self.help_menu = QMenu("Help", self)

        self._add_actions()

        self.file_menu.triggered[QAction].connect(parent.controller.triggerMenuFile)    
        self.edit_menu.triggered[QAction].connect(parent.controller.triggerMenuEdit)    
        self.help_menu.triggered[QAction].connect(parent.controller.triggerMenuHelp)    

        # povezivanje menija
        self._populate_menues()

    def _add_actions(self):
        self.file_menu.addAction("New file")
        self.file_menu.addAction("New container")
        self.file_menu.addAction("New window")
        self.file_menu.addSeparator()
        self.file_menu.addAction("Save")
        self.file_menu.addAction("Save as")
        self.file_menu.addAction("Export")
        self.file_menu.addAction("Import")
        self.file_menu.addAction("Print")
        self.file_menu.addSeparator()
        self.file_menu.addAction("Exit")

        self.edit_menu.addAction("Undo")
        self.edit_menu.addAction("Redo")
        self.edit_menu.addSeparator()
        self.edit_menu.addAction("Cut")
        self.edit_menu.addAction("Copy")
        self.edit_menu.addAction("Paste")
        self.edit_menu.addSeparator()
        self.edit_menu.addAction("Find")
        self.edit_menu.addAction("Find and replace")

        self.help_menu.addAction("Get started")
        self.help_menu.addAction("Guide")
        self.help_menu.addSeparator()
        self.help_menu.addAction("About us")


    def _populate_menues(self):
        self.addMenu(self.file_menu)
        self.addMenu(self.edit_menu)
        self.addMenu(self.help_menu)
