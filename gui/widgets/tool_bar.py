from tkinter.filedialog import SaveAs
from PySide2.QtWidgets import QToolBar, QAction
from PySide2.QtGui import QIcon

class Toolbar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        self.parent = parent

        #TODO uvezivanje akcija sa kontrolerom
        self.addAction(QAction(QIcon("resources/icons/document--plus.png"),"Create new", self))
        self.addAction(QAction(QIcon("resources/icons/folder--plus.png"),"Create a new folder", self))
        self.addSeparator()

        save_all = QAction(QIcon("resources/icons/disks.png"),"Save all", self)
        save_all.setStatusTip("Save all")
        save_all.triggered.connect(self.parent.controller.saveChangesAll)
        self.addAction(save_all)
        
        self.addAction(QAction(QIcon("resources/icons/disk.png"),"Save selected", self))
        self.addAction(QAction(QIcon("resources/icons/minus.png"),"Delete selected file/table/collection", self))
        self.addSeparator()

        self.addAction(QAction(QIcon("resources/icons/document-import.png"),"Import", self))
        self.addAction(QAction(QIcon("resources/icons/document-export.png"),"Export", self))
        self.addSeparator()
        self.addAction(QAction(QIcon("resources/icons/arrow-circle-315-left.png"),"Undo", self))
        self.addAction(QAction(QIcon("resources/icons/arrow-circle-315.png"),"Redo", self))
        self.addSeparator()
        self.addAction(QAction(QIcon("resources/icons/document-page.png"),"First", self))
        self.addAction(QAction(QIcon("resources/icons/document-page-previous.png"),"Previous", self))
        self.addAction(QAction(QIcon("resources/icons/document-page-next.png"),"Next", self))
        self.addAction(QAction(QIcon("resources/icons/document-page-last.png"),"Last", self))