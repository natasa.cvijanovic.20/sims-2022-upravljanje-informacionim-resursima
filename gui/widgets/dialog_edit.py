from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QDialog, QFormLayout, QLabel, QLineEdit, QDialogButtonBox, QMessageBox

class DialogEdit(QDialog):
    def __init__(self, parent=None, header_data=[], fill_data = [], current_row = 0):
        super().__init__()
        
        self.setWindowTitle("Rukovalac Informacionim Resursima - Edit")
        self.setWindowIcon(QIcon("resources/icons/document--pencil.png"))
        self.resize(400, 100)

        self.parent = parent
        self.current_row = current_row

        self.header_data = []
        for i, a in enumerate(header_data):
            if i > 0:
                self.header_data.append(a)
            
        self.old_data = fill_data
        self.fill_data = []
        for a in fill_data:
            self.fill_data.append(a)
        if len(self.header_data) != len(self.fill_data):
            self.fill_data.pop(0)

        self.form_layout = QFormLayout(self)
        self.info_label = QLabel("Enter the values:")
        self.form_layout.addRow(self.info_label)

        self.first, self.second, self.third, self.fourth, self.fifth, self.sixth = None, None, None, None, None, None
        self.fields= [self.first, self.second, self.third, self.fourth, self.fifth, self.sixth]

        self.first_l, self.second_l, self.third_l, self.fourth_l, self.fifth_l, self.sixth_l = None, None, None, None, None, None
        self.labels= [self.first_l, self.second_l, self.third_l, self.fourth_l, self.fifth_l, self.sixth_l]

        for i, column in enumerate(self.header_data):
            self.labels[i] = QLabel(column)
            self.fields[i] = QLineEdit()
            self.fields[i].setText(self.fill_data[i])

            self.form_layout.addRow(self.labels[i], self.fields[i])

        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.form_layout.addRow(self.button_box)
        self.setLayout(self.form_layout)

    # potvrda izmene ir
    def accept(self):
        for i in range(len(self.fill_data)):
            self.fill_data[i] = self.fields[i].text()
        
        
        comparison = []
        for a in self.old_data:
            comparison.append(a)
        if len(self.fill_data) != len(self.old_data):
            comparison.pop(0)

        flag = False
        for old, new in zip(comparison, self.fill_data):
            if old != new:
                flag = True
                break
        
        if flag:
            ra = ""         
            if len(self.fill_data) != len(self.old_data):
                ra = self.old_data[0]

            f, dat = self.parent.checkDataArr(self.fill_data, self.header_data)
            
            if (not self.parent.checkKeyValidity(self.fill_data, ra)) and (comparison[0] != self.fill_data[0]):
                QMessageBox.warning(self, "Failed", "Unique fields must be respected")
            else:
                if f:
                    QMessageBox.information(self, "Successfully edited", "Success!")
                else:
                    QMessageBox.warning(self, "Successfully edited, warning", "Successfully edited, but some data has been entered incorrectly. The invalid values have been overwritten")

                self.parent.editIR(dat, self.current_row, ra)
        else:
            QMessageBox.information(self, "Invalid", "Data is the same as before")

        self.close()