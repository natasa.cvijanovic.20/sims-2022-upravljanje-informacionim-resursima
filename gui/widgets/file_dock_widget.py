from PySide2.QtWidgets import QDockWidget, QFileSystemModel, QTreeView
from PySide2.QtCore import QDir, QObject, SIGNAL

class FileDockWidget(QDockWidget):
    def __init__(self, title="", parent=None):
        def row_clicked(index):
            '''
            when a row is clicked... show the name
            '''
            self.parent().controller.populateModel(self.file_model.filePath(index), False)

        super().__init__(title, parent)
        
        path = QDir.currentPath() + "\data"
        
        self.file_model = QFileSystemModel()
        self.file_model.setRootPath(path)
        

        self.tree_view = QTreeView(parent)
        self.tree_view.setModel(self.file_model)
        self.tree_view.setAlternatingRowColors(True)
        self.tree_view.setRootIndex(self.file_model.index(path))
        self.tree_view.hideColumn(1)
        self.tree_view.hideColumn(2)
        self.tree_view.hideColumn(3)

        QObject.connect(self.tree_view, SIGNAL("clicked (QModelIndex)"), row_clicked)

        self.setWidget(self.tree_view)
