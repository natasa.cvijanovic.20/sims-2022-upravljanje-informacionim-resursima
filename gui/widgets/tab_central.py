from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableWidget, QTableWidgetItem, QAbstractItemView
from PySide2.QtCore import QObject, SIGNAL, Qt

from .tool_bar_specific import ToolbarSpecific

class TabCentral(QWidget):
    def __init__(self, controller, model_data, parent=None, title=""):
        def getHeaderDataa():
            header_data = self.controller.getHeaderData(self.objectName())
            if not "." in self.objectName():
                h_d = []
                for dicti in header_data:
                    h_d.append(dicti["Field"])
                return h_d
            return header_data

        super().__init__(parent=parent)
        
        self.setObjectName(title)
        self.controller = controller
        self.model_data = model_data
        self.header_data = getHeaderDataa()

        self.to_create = []
        self.to_update = []
        self.to_delete = []

        self.ao = QVBoxLayout(self)
        self.tool_bar = ToolbarSpecific("Second toolbar", self)
        self.tableWidget = None
        if self.model_data:
            self.tableWidget = QTableWidget(len(self.model_data), len(model_data[0].keys()), self)
            self.tableWidget.setHorizontalHeaderLabels(self.model_data[0].keys())

            for row in range(self.tableWidget.rowCount()):
                for i, column in enumerate(self.model_data[0].keys()):
                    item = QTableWidgetItem(0)
                    item.setText(str(self.model_data[row][column]))
                    # if column == "id":
                    item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
                    self.tableWidget.setItem(row, i, item)
            self.tableWidget.resizeColumnsToContents()

        else:
            if not "." in self.objectName():
                self.tableWidget = QTableWidget(0, len(self.header_data),self)
                self.tableWidget.setHorizontalHeaderLabels(self.header_data)
            self.tableWidget = QTableWidget(0, len(self.header_data),self)
            self.tableWidget.setHorizontalHeaderLabels(self.header_data)
        
        

        self.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ao.addWidget(self.tool_bar)
        self.ao.addWidget(self.tableWidget)

        self.setLayout(self.ao)

