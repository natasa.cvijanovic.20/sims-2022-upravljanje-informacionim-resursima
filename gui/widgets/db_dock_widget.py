from PySide2.QtWidgets import QDockWidget, QListWidget, QListWidgetItem
from PySide2.QtCore import QObject, SIGNAL

class DBDockWidget(QDockWidget):
    def __init__(self, title="", parent=None):
        def row_clicked(index):
            '''
            when a row is clicked... show the name
            '''
            self.parent().controller.populateModel(self.list_w.model().data(index), True)

        super().__init__(title, parent)

        self.list_w = QListWidget(self)

        table_names = self.parent().controller.getAllTableNames()
        for table_name in table_names:
            QListWidgetItem(table_name, self.list_w)

        self.setWidget(self.list_w)

        QObject.connect(self.list_w, SIGNAL("clicked (QModelIndex)"), row_clicked)