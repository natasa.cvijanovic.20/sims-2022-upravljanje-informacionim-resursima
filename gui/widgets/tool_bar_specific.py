from PySide2.QtWidgets import QToolBar, QAction
from PySide2.QtGui import QIcon

class ToolbarSpecific(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        self.parent = parent

        create_new = QAction(QIcon("resources/icons/blue-document.png"),"Create new", self)
        create_new.setStatusTip("Create new")
        create_new.triggered.connect(self.parent.controller.triggerToolBarSpecCreate)
        self.addAction(create_new)

        edit_selected = QAction(QIcon("resources/icons/document--pencil.png"),"Edit selected", self)
        edit_selected.setStatusTip("Edit selected")
        edit_selected.triggered.connect(self.parent.controller.triggerToolBarSpecEdit)
        self.addAction(edit_selected)

        delete_selected = QAction(QIcon("resources/icons/document--minus.png"),"Delete selected", self)
        delete_selected.setStatusTip("Delete selected")
        delete_selected.triggered.connect(self.parent.controller.triggerToolBarSpecDelete)
        self.addAction(delete_selected)

        save_selected = QAction(QIcon("resources/icons/disk-black.png"),"Save", self)
        save_selected.setStatusTip("Save selected")
        save_selected.triggered.connect(self.parent.controller.triggerToolBarSpecSave)
        self.addAction(save_selected)
        