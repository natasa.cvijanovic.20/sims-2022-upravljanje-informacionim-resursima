from PySide2.QtWidgets import QWidget, QVBoxLayout, QTabWidget, QMessageBox
from PySide2.QtCore import QObject, SIGNAL

class CentralWidget(QWidget):
    def __init__(self, parent=None, controller = None):
        def closeTab(index):
            w = self.tabs.widget(index)
            # provera da li postoje nesacuvane izmene
            if (w.to_create ==[] and w.to_update==[] and w.to_delete==[]):
                self.tabs.removeTab(index)
            else:
                # cuvanje izmena, zatvaranje aplikacije
                reply = QMessageBox.question(self, 'Tab Close', 'You have unsaved changes, are you sure you want to close the tab?',
				QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel, QMessageBox.Cancel)

                if reply == QMessageBox.Save:
                    self.controller.saveCurrent(self.tabs.widget(index))
                    self.tabs.removeTab(index)
                elif reply == QMessageBox.Discard:
                    self.tabs.removeTab(index)

        super().__init__(parent)
        
        self.controller = controller
        self.layout = QVBoxLayout(self)
        
        # inicijalizacija tabova
        self.tabs = QTabWidget()
        self.tabs.setTabsClosable(True)

        QObject.connect(self.tabs, SIGNAL('tabCloseRequested(int)'), closeTab)

        # dodavanje tabova na widget
        self.layout.addWidget(self.tabs)
        self.setLayout(self.layout)
    