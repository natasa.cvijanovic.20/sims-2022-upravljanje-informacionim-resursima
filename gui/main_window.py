from datetime import datetime
from PySide2.QtWidgets import QMainWindow, QLabel, QMessageBox, QDockWidget
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt

from controller.controller import Controller

from gui.widgets.menu_bar import MenuBar
from gui.widgets.tool_bar import Toolbar
from gui.widgets.status_bar import StatusBar
from gui.widgets.central_widget import CentralWidget
from gui.widgets.file_dock_widget import FileDockWidget
from gui.widgets.db_dock_widget import DBDockWidget

from data_manager.csv_file_manager import CSVFileManager
from data_manager.mysql_db_manager import MySqlDBManager

class MainWindow(QMainWindow):
    def __init__(self, parent=None, user=None):
        super().__init__()
        # osnovna podesavanja
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icons/ui-scroll-pane-blog.png"))
        self.resize(1000, 700)

        
        self.base_managers = [CSVFileManager("data/users.csv"), MySqlDBManager("user")]
        #inicijalizacija kontrolera za prozor
        self.controller = Controller(self, self.base_managers)
        # inicijalizacija osnovnih elemenata guia
        self.menu_bar = MenuBar(self)
        self.tool_bar = Toolbar(title="ToolBar",parent=self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self, self.controller)
        self.file_dock_widget = FileDockWidget("Struktura - fajlovi",self)
        self.db_dock_widget = DBDockWidget("Struktura - baza",self)
        
        self.file_dock_widget.setFeatures(QDockWidget.DockWidgetMovable)
        self.db_dock_widget.setFeatures(QDockWidget.DockWidgetMovable)

        # uvezivanje elemenata guia
        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.db_dock_widget, Qt.Vertical)

        # cuvanje prijavljenog kor
        self.user = user
        self.status_bar.addWidget(QLabel("Current user: " + self.user["user_id"].upper() + "      Logged in since: " + datetime.now().strftime("%H:%M:%S") ))
    
    def closeEvent(self, event):
        if self.controller.checkForChanges():
            reply = QMessageBox.question(self, 'Window Close', 'You have unsaved changes, are you sure you want to close the window?',
                    QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel, QMessageBox.Cancel)

            self.controller.azurirajKontekst()

            if reply == QMessageBox.Save:
                self.controller.saveChangesAll()
                event.accept()
            elif reply == QMessageBox.Discard:
                QMessageBox.information(self, "Application closing", "Changes not saved; Have a nice day!")
                event.accept()
            else:
                event.ignore()
        else:
            QMessageBox.information(self, "Application closing", "Have a nice day!")
            event.accept()
 
