import sys

from authentication.model.login_model import LoginModel
from authentication.ui.login_view import LoginView
from authentication.controller.login_controller import LoginController
from data_manager.csv_file_manager import CSVFileManager
from data_manager.mysql_db_manager import MySqlDBManager

class AplikacijaRukovalacInformacionimResursima():
    def __init__(self, id, name, logotip, administracija_omogucena=False):
        self.id = id
        self.name = name
        self.logotip = logotip
        self.administracija_omogucena = administracija_omogucena

        self.base_managers = [CSVFileManager("data/users.csv"), MySqlDBManager("user")]
        self.login_controller = LoginController(parent=self, managers=self.base_managers)
        self.user_model = LoginModel()
        self.login_window = LoginView(parent=self,model = self.user_model, controller=self.login_controller)

        self.result = self.login_window.exec_()

    def getResult(self):
        return self.result

    #FIXME main_windows bi trebalo da je niz prozora
    def setMain(self, main):
        self.main_window = main

    def reject(self, source):
        sys.exit()
