from PySide2.QtWidgets import QTableWidgetItem, QMessageBox
from PySide2.QtCore import Qt

from data_manager.mysql_db_manager import MySqlDBManager
from gui.widgets.dialog_create import DialogCreate
from gui.widgets.dialog_edit import DialogEdit
from gui.widgets.tab_central import TabCentral

class Controller():
    def __init__(self, parent = None, managers = None):
        self.parent = parent
        self.managers = managers

    # TODO obaviti azuriranje konteksta aplikacije i korisnika; 
    def azurirajKontekst(self):
      pass

    #TODO reagovanje na dogadjaje iz menija i toolbara
    def triggerMenuFile(self,q):
      print (q.text()+" is triggered")
      if q.text() == "Exit":
        self.parent.close()

    #TODO reagovanje na dogadjaje iz menija i toolbara
    def triggerMenuEdit(self,q):
      print (q.text()+" is triggered")
      if q.text() == "Exit":
        self.parent.close()

    #TODO reagovanje na dogadjaje iz menija i toolbara
    def triggerMenuHelp(self,q):
      print (q.text()+" is triggered")
      if q.text() == "Exit":
        self.parent.close()

    #TODO izmena lozinke
    def passwordChange(self, new_password):
      pass

    # provera da li postoje izmene
    def checkForChanges(self):
      tabs, current_tab = self.getSelectedTab()
      
      for i in range(tabs.count()):
        if (tabs.widget(i).to_create !=[] or tabs.widget(i).to_update!=[] or tabs.widget(i).to_delete!=[]):
          return True

      return False

    # sacuvati sve izmene
    def saveChangesAll(self):
      tabs, current_tab = self.getSelectedTab()
      try:
        for i in range(tabs.count()):
          self.saveCurrent(tabs.widget(i), True)
        QMessageBox.information(self.parent, "Saved", "Success! Saved everything")
      except: 
        QMessageBox.information(self.parent, "Failed", "Something went wrong, failed to save data. Please try again")

    # dogadjaj koji se aktivira klikom na tulbar dugme za pravljenje novog ir
    def triggerToolBarSpecCreate(self, q):
      tabs, current_tab = self.getSelectedTab()
      create_dialog = DialogCreate(parent=self, header_data=current_tab.header_data)
      create_dialog.exec_()
    
    # ubacuje u tabelarni prikaz kreirani ir
    def createIR(self, data):
      tabs, current_tab = self.getSelectedTab()
      current_tab.to_create.append(data)
      h_d = current_tab.header_data
      row = current_tab.tableWidget.rowCount()

      current_tab.tableWidget.insertRow(current_tab.tableWidget.rowCount())
      for i, column in enumerate(h_d):
        item = QTableWidgetItem(0)
        if column in data.keys():
          item.setText(str(data[column]))
          item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)

          current_tab.tableWidget.setItem(row, i, item)

    # dogadjaj koji se aktivira klikom na tulbar dugme za izmenu ir
    def triggerToolBarSpecEdit(self, q):
      tabs, current_tab = self.getSelectedTab()
      row = current_tab.tableWidget.currentRow()
      if row >= 0:
        fill_data = []
        for column in range(current_tab.tableWidget.columnCount()):
            it = current_tab.tableWidget.item(row, column)
            try:
              fill_data.append(it.text())
            except:
              fill_data.append("")
        if len(fill_data) > 1:
          edit_dialog = DialogEdit(parent=self, header_data=current_tab.header_data, fill_data=fill_data, current_row = row)
          edit_dialog.exec_()
        else:
          QMessageBox.information(self.parent, "Not editable", "This IR doesnt support editing")
      else:
        QMessageBox.information(self.parent, "Row not selected", "Selection is empty; please select a row")
    
    # ubacivanje promenjenog ir u tabelarni prikaz
    def editIR(self, fill_data, current_row, id_value):
      tabs, current_tab = self.getSelectedTab()
      h_d = current_tab.header_data
      up = [id_value]
      for ra in fill_data:
        up.append(ra)
      current_tab.to_update.append(up)
      for i, h in enumerate(h_d):
        if h != "id":
          it = current_tab.tableWidget.item(current_row, i).setText(up[i])
    
    # check for double values
    def checkKeyValidity(self, data, index):
      if index:
        column = 1
      else:
        column = 0
      
      tabs, current_tab = self.getSelectedTab()
      for j in range(current_tab.tableWidget.rowCount()):
        it = current_tab.tableWidget.item(j, column)
        if type(data) == dict:
          for i, key in enumerate(data.keys()):
            if i > 0:
              break
            if data[key] == it.text():
              return False
        else:
          if data[0] == it.text():
            return False
      return True

    # brisanje selektovanog reda iz tabelarnog prikaza
    def triggerToolBarSpecDelete(self, q):
      tabs, current_tab = self.getSelectedTab()
      row = current_tab.tableWidget.currentRow()
      if row >= 0:
        data = []
        for column in range(current_tab.tableWidget.columnCount()):
          it = current_tab.tableWidget.item(row, column)
          try:
            data.append(it.text())
          except:
            data.append("")
        current_tab.tableWidget.removeRow(row)
        current_tab.to_delete.append(data)
      else:
        QMessageBox.information(self.parent, "Row not selected", "Selection is empty; please select a row")

    # dogadjaj koji se aktivira klikom na tulbar dugme za cuvanje ir
    def triggerToolBarSpecSave(self, q):
      tabs, current_tab = self.getSelectedTab()
      self.saveCurrent(current_tab=current_tab)

    # sacuvaj trenutni tab
    def saveCurrent(self, current_tab, multi = False):
      for manager in self.managers:
        manager.root = current_tab.objectName()
      try:
        self.saveCreatedCurrent(current_tab)
        self.saveEditedCurrent(current_tab)
        self.saveDeletedCurrent(current_tab)
        if not multi:
          name = " "
          if "." in current_tab.objectName():
            name += "file: " + current_tab.objectName().rsplit("/", 1)[1]
          else:
            name += "table: " + current_tab.objectName()
          QMessageBox.information(self.parent, "Saved", "Success! Saved" + name)
      except:
        if not multi:
          QMessageBox.information(self.parent, "Failed", "Something went wrong, failed to save data. Please try again")

    # sacuvaj novi ir iz trenutnog taba
    def saveCreatedCurrent(self, current_tab):
      if "." in current_tab.objectName():
        for dic in current_tab.to_create:
          self.managers[0].createNewIR(self.cleanDataDict(dic), current_tab.header_data)
      else:
        for dic in current_tab.to_create:
          self.managers[1].createNewIR(self.cleanDataDict(dic), current_tab.header_data)
      current_tab.to_create.clear()

    # trajno cuvanje izmena
    def saveEditedCurrent(self, current_tab):
      if "." in current_tab.objectName():
        for arr in current_tab.to_update:
          self.managers[0].updateIR(self.cleanDataArr(arr), current_tab.header_data)
      else:
        for arr in current_tab.to_update:
          self.managers[1].updateIR(self.cleanDataArr(arr), current_tab.header_data)
      
      current_tab.to_update.clear()
    
    # trajno cuvanje brisanja
    def saveDeletedCurrent(self, current_tab):
      if "." in current_tab.objectName():
        for arr in current_tab.to_delete:
          self.managers[0].deleteIR(self.cleanDataArr(arr), current_tab.header_data)
      else:
        for arr in current_tab.to_delete:
          self.managers[1].deleteIR(self.cleanDataArr(arr), current_tab.header_data)
      
      current_tab.to_delete.clear()

    # proverava validnost unetih podataka
    def checkDataArr(self, data, headers):
      flag = True
      coun = 0
      if len(headers) > len(data):
        coun = 1
      for i, d in enumerate(data):
        if "type" in headers[i+coun] or "registered" in headers[i+coun]:
          try:
            int(d)
          except:
            data[i] = "0"
            flag = False
      return flag, data 

    # proverava validnost unetih podataka
    def checkDataDict(self, data):
      flag = True
      keys = data.keys()
      for key in keys:
        if "type" in key or "registered" in key:
          try:
            int(data[key])
          except:
            data[key] = "0"
            flag = False
      return flag, data

    # cisti niz podatke - ',' i ';'
    def cleanDataArr(self, data):
      for d in data:
        output = ""
        for char in d:
          if char != "," and char != ";":
            output += char
      return data

    # cisti recnik podatke - ',' i ';'
    def cleanDataDict(self, data):
      keys = data.keys()
      for k in keys:
        output = ""
        for char in data[k]:
          if char != "," and char != ";":
            output += char
      return data

    # vraca tab roditelja i tab koji je trenutno selektovan
    def getSelectedTab(self):
      tabs = self.parent.central_widget.tabs
      current_tab = tabs.widget(tabs.currentIndex())
      return tabs, current_tab
      
    # preusmeravanje u zavisnosti od tipa izvora podataka
    def populateModel(self, source_name, is_database = False):
      if is_database:
        self.populateModelFromDatabase(source_name)
      else:
        self.populateModelFromFile(source_name)
    
    # populisanje baze iz csv fajla
    def populateModelFromFile(self, source_file_name):
      name = source_file_name.rsplit('/', 1)[1]
      flag = True
      for i in range(self.parent.central_widget.tabs.count()):
        if self.parent.central_widget.tabs.widget(i).objectName() == source_file_name:
          self.parent.central_widget.tabs.setCurrentIndex(i)
          flag = False
          break
      if flag:
        self.managers[0].root = source_file_name
        data = self.managers[0].getAllData()
        a = TabCentral(controller=self, model_data = data, parent=self.parent.central_widget, title=source_file_name)

        self.parent.central_widget.tabs.addTab(a, name)
        self.parent.central_widget.tabs.setCurrentIndex(self.parent.central_widget.tabs.count()-1)

    # populisanje modela iz baze podataka
    def populateModelFromDatabase(self, source_table_name):
      flag = True
      for i in range(self.parent.central_widget.tabs.count()):
        if self.parent.central_widget.tabs.widget(i).objectName() == source_table_name:
          self.parent.central_widget.tabs.setCurrentIndex(i)
          flag = False
          break
      if flag:
        self.managers[1].root = source_table_name
        data = self.managers[1].getAllData()
        a = TabCentral(controller=self, model_data = data, parent=self.parent.central_widget, title=source_table_name)

        self.parent.central_widget.tabs.addTab(a, source_table_name)
        self.parent.central_widget.tabs.setCurrentIndex(self.parent.central_widget.tabs.count()-1)

    # dobavi podatke iz zaglavlja 
    def getHeaderData(self, source):
      res = []
      if "." in source:
        res = self.managers[0].getHeaderData(source)
      else:
        res = self.managers[1].getHeaderData(source)
      return res

    # vracanje naziva svih tabela unutar baze podataka
    def getAllTableNames(self):
      for data_manager in self.managers:
        if type(data_manager) == MySqlDBManager:
          return data_manager.getTableNames()