from abc import ABC

class DataManager(ABC):
    def __init__(self, source = None):
        self.root = source
        super().__init__()
     
    #return all data
    def getAllData(self):
        return None
        
    #return all data from a specific entity
    def getWithParams(self, params=None):
        # params: [(a,b), (c,d)]
        return params

    # return columns
    def getHeaderData(self, source = None):
        return source

    # insert new IR 
    def createNewIR(self, data, headers):
        return None
    
    # update IR
    def updateIR(self, data, headers):
        return None

    # delete IR
    def deleteIR(self, data):
        return

