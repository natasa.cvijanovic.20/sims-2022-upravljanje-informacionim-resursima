from csv import DictReader
import mysql.connector

from .data_manager import DataManager

class MySqlDBManager(DataManager):
    def __init__(self, source, db_connection_info=None):
        super().__init__(source)

        self.db_login = db_connection_info
        if self.db_login is None:
            with open("data/database_login.csv", "r", encoding="utf-8") as f:
                reader = DictReader(f)
                for row in reader:
                    self.db_login = row

        self.db = mysql.connector.connect(**self.db_login)

        self.cur = self.db.cursor(dictionary=True)
    
    #return all data from a table from the db
    def getAllData(self):
        query = "select * from " + self.root
        self.cur.execute(query)

        table_data = []
        for row in self.cur.fetchall():
            table_data.append(row)

        return table_data

    # return all data from a specific entity, from mysql db
    def getWithParams(self, params=None):
        query = "select * from " + self.root
        # params: [(a,b), (c,d)]
        if params:
            query += " where "
            for p in params:
                query += p[0] + "=" + "'" + p[1] + "'" + " and "
            query = query.rsplit(' ', 2)[0]

        self.cur.execute(query)

        user = self.cur.fetchone()
        return user

    # return all table names
    def getTableNames(self):
        self.cur.execute("show tables")
        
        res = []
        for r in self.cur.fetchall():
            res.append(r["Tables_in_" + self.db_login["database"]])
        
        return res

    # return columns from a table
    def getHeaderData(self, source):
        query = "describe " + source

        self.cur.execute(query)
        return self.cur.fetchall()
    
    # insert new IR
    def createNewIR(self, data, headers):
        keys = data.keys()

        query = "insert into " + self.root + " ("
        for key in keys:
            query += key + ","
        query = query.rsplit(",", 1)[0]
        query += ") values ("
        for key in keys:
            if "0123456789" not in data[key]:
                query += "'" + data[key] + "'"
            else:
                query += data[key]
            query += ","
        query = query.rsplit(",", 1)[0]
        query += ")"

        self.cur.execute(query)
        self.db.commit()
        
    # update IR
    def updateIR(self, data, headers):
        # update user set 
        query = "update " + self.root + " set "

        for i, header in enumerate(headers):
            if i > 0:
                if "0123456789" not in data[i]:
                    query += header + "='" + data[i] + "',"
                else:
                    query += header + "=" + data[i] + ","
        
        # update user set user_id = "a", user_password = "eu", user_registered = 1, user_type = 3,
        query = query.rsplit(",", 1)[0]
        # update user set user_id = "a", user_password = "eu", user_registered = 1, user_type = 3
        query += " where " + headers[0] + "=" + data[0]

        self.cur.execute(query)
        self.db.commit()

    # delete IR
    def deleteIR(self, data, headers):
        query = "delete from " + self.root + " where " + headers[0] + " = " + data[0]

        self.cur.execute(query)
        self.db.commit()