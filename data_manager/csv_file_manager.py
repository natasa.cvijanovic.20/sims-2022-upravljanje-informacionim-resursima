from csv import DictReader, reader, writer

from .data_manager import DataManager

class CSVFileManager(DataManager):
    def __init__(self,source):
        super().__init__(source)
    
    #return all data from a file and a table from the db
    def getAllData(self):
        file_data = []
        with open(self.root, "r", encoding="utf-8") as f:
            reader = DictReader(f)
            for row in reader:
                file_data.append(row)

        return file_data

    # return all data from a specific entity, from a file
    def getWithParams(self, params=None):
        user = None
        with open(self.root, "r", encoding="utf-8") as f:
            reader = DictReader(f)
            for row in reader:
                flag = False
                # params: [(a,b), (c,d)]
                for p in params:
                    if row[p[0]] != p[1]:
                        flag = True
                        break
                if not flag:
                    user = row
        return user

    # return columns
    def getHeaderData(self, source):
        self.root = source
        with open(self.root, "r", encoding="utf-8") as f:
            r = reader(f, delimiter=",")
            for row in r:
                return row

    # insert new IR
    def createNewIR(self, data, headers):
        with open(self.root, "a", encoding= "utf-8") as f:
            writer_csv = writer(f)
            writer_csv.writerow(data.values())
    
    # update IR
    def updateIR(self, data, headers):
        with open(self.root, "r", encoding="utf-8") as f:
            lines = []
            reader_csv = reader(f)
            for row in reader_csv:
                if len(row) > 0:
                    if row[0] == data[0]:
                        new_line = []
                        for i in range(len(row)):
                            new_line.append(data[i])
                        row = new_line
                    lines.append(row)

        with open(self.root, "w", encoding="utf-8") as f:
            writer_csv = writer(f)
            writer_csv.writerows(lines)
     
    # delete IR
    def deleteIR(self, data, headers):
        with open(self.root, "r", encoding="utf-8") as f:
            lines = []
            reader_csv = reader(f)
            for row in reader_csv:
                if len(row) > 0:
                    if row[0] != data[0]:
                        lines.append(row)

        with open(self.root, "w", encoding="utf-8") as f:
            writer_csv = writer(f)
            writer_csv.writerows(lines)