from model.resurs import Resurs

class Kolekcija(Resurs):
    def __init__(self, naziv, autor, radni_prostor, kolekcija=None, atributi = [],resursi=[]):
        super().__init__(naziv, autor, radni_prostor, kolekcija, atributi)
        # u resurse spadaju i druge kolekcije i informacioni resursi
        self.resursi = resursi

    @property
    def broj_elemenata(self):
        return len(self.resursi)

    @property
    def parent(self):
        return self.kolekcija if self.kolekcija else self.radni_prostor

    @property
    def children(self):
        return self.resursi, self.atributi

    @property
    def children_kolekcija(self):
        pass
    
    def dodaj_resurs(self, resurs):
        self.resursi.append(resurs)
        resurs.kolekcija = self