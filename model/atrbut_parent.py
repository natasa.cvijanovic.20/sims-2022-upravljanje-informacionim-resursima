from abc import ABC


class AtributParent(ABC):
    def __init__(self, atributi = []):
        super().__init__()
        self.atributi = atributi

    def dodaj_atribut(self, atribut):
        self.atributi.append(atribut)
        atribut.atribut_parent = self