from model.atrbut_parent import AtributParent
from model.atribut import Atribut


class AtributKolekcija(Atribut, AtributParent):
    def __init__(self, naziv, vrednost, atribut_parent, atributi=...):
        super().__init__(naziv, vrednost, atribut_parent)
        self.atributi = atributi

    @property
    def children(self):
        return self.atributi