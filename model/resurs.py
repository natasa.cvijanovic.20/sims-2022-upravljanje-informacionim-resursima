from abc import ABC

from model.atrbut_parent import AtributParent

class Resurs(AtributParent, ABC):
    def __init__(self, naziv, radni_prostor=None, kolekcija=None, atributi = []):
        super().__init__(atributi)
        self.naziv = naziv
        self.radni_prostor = radni_prostor
        self.kolekcija = kolekcija

    @property
    def parent(self):
        return self.kolekcija if self.kolekcija else self.radni_prostor

    @property
    def children(self):
        return self.atributi
    
    def __repr__(self):
        "RESURS - %s"% (self.naziv,)