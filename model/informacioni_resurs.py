from .resurs import Resurs

class InformacioniResurs(Resurs):
    def __init__(self, naziv, radni_prostor, kolekcija, atributi=[]):
        super().__init__(naziv=naziv, radni_prostor=radni_prostor, kolekcija=kolekcija, atributi=atributi)

    @property
    def parent(self):
        return self.kolekcija

    @property
    def children(self):
        return self.atributi

    #nije neophodno definisati
    @parent.setter 
    def parent(self, value):
        self.kolekcija = value

    #TODO ispis podataka
    def ispisiPodatke():
        raise NotImplementedError()