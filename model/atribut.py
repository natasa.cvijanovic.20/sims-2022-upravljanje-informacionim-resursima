from abc import ABC

class Atribut(ABC):
    def __init__(self, naziv, vrednost, atribut_parent):
        super().__init__()
        self.naziv = naziv
        self.vrednost = vrednost
        self.atribut_parent = atribut_parent

    @property
    def parent(self):
        return self.atribut_parent

    @property
    def children(self):
        return None