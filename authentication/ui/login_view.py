from PySide2.QtGui import QIcon
from PySide2.QtWidgets import QDialog, QFormLayout, QLabel, QLineEdit, QDialogButtonBox, QMessageBox

from authentication.model.login_model import LoginModel

class LoginView(QDialog):
    def __init__(self, parent=None, model=None, controller=None):
        super().__init__()

        
        self.setWindowTitle("Rukovalac Informacionim Resursima - Log in")
        self.setWindowIcon(QIcon("resources/icons/lock.png"))
        self.resize(400, 100)

        #model je potrebno da bude tipa loginmodel
        if model is None:
            model = LoginModel()

        self.parent = parent

        self.login_model = model
        self.login_controller = controller

        self.form_layout = QFormLayout(self)
        self.info_label = QLabel("Enter your username and password")
        self.username_label = QLabel("Username*")
        self.password_label = QLabel("Password*")
        self.username_input = QLineEdit()
        self.password_input = QLineEdit()
        self.info_add_label = QLabel("*necessary fields")

        self.password_input.setEchoMode(QLineEdit.Password)

        self.button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        # popunjavanje forme spram modela
        if model.user_id is not None:
            self.username_input.setText(model.user_id)
        if model.user_password is not None:
            self.password_input.setText(model.user_password)

        self.button_box.accepted.connect(self.accept)
        self.button_box.rejected.connect(self.reject)

        self.form_layout.addRow(self.info_label)
        self.form_layout.addRow(self.username_label, self.username_input)
        self.form_layout.addRow(self.password_label, self.password_input)
        self.form_layout.addRow(self.info_add_label)
        self.form_layout.addRow(self.button_box)

        self.setLayout(self.form_layout)

    #prenosenja signala zatvaranja aplikacije kontroleru
    def reject(self):
        self.login_controller.quit()

    #potvrda unosa kredencijala
    def accept(self):
        # pokupimo podatke iz forme
        self.login_model.user_id = self.username_input.text()
        self.login_model.user_password = self.password_input.text()

        # prosledimo podatke login kontroleru (preko metode login)
        if self.username_input.text() == "" or self.password_input.text() == "":
            QMessageBox.warning(self, "Unable to log in", "Username/password can not be empty")
        else:
            result = self.login_controller.login(self.login_model)
            if result:
                #ukoliko korisnik postoji, ali nije registrovan
                QMessageBox.warning(self, "Unable to log in", "The administration has not yet approved your ability to use the application. Please contact your administrator")
                self.username_input.clear()
                self.password_input.clear()
            elif result is False:
                #ukoliko korisnik ne postoji
                QMessageBox.warning(self, "Unable to log in", "The username or the password is incorrect")
                self.password_input.clear()