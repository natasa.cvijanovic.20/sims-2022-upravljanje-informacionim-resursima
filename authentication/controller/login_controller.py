from gui.main_window import MainWindow

class LoginController():
    def __init__(self, parent, managers):
        self.parent = parent
        
        self.managers = managers

    def login(self, user_model):
        # pozovemo skladiste da proverimo da li je korisnik registrovan

        res = None
        for db_m in self.managers:
            if not res:
                res = db_m.getWithParams([("user_id",user_model.user_id), ("user_password", user_model.user_password)])

        #ukoliko korisnik postoji i registrovan je, pokrenuti aplikaciju
        if res and int(res["user_registered"]):
            self.start_app(res)
        #ukoliko korisnik postoji, ali nije registrovan
        elif res:
            return True
        #ukoliko korisnik ne postoji
        else:
            return False

    #prenosenja signala aplikaciji za njeno zatvaranje
    def quit(self):
        self.parent.reject(self)

    #pokretanje korisnickog interfejsa
    def start_app(self, user=None):
        print("Logovanje")
        print("Zdravo ", user["user_id"])
        print("Ucitavanje prozora...")
        self.parent.setMain(MainWindow(user=user, parent=self.parent))
        self.parent.main_window.show()
        self.parent.login_window.done(0)