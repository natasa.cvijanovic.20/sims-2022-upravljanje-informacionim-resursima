from app import AplikacijaRukovalacInformacionimResursima
from PySide2.QtWidgets import QApplication
import sys

if __name__ == "__main__":
    application = QApplication(sys.argv)
    
    app = AplikacijaRukovalacInformacionimResursima(1, "Rukovalac informacionim resursima", "")
    
    sys.exit(application.exec_())
